# Usage Guide
### Generating Heatmaps
```shell script
python main.py generate --vid <input_video> --out <output_file_pattern>
```
There are several output files, so the -o specifies a base name to be used in naming the files.

### Detecting wrists
```shell script
python main.py detect --vid <input_video> --in <input_file_pattern> --out <output_file>
```
The input file pattern should match the pattern given when generating.