import copy
import glob
import os
import types
from warnings import warn

import cv2
import numpy as np

TOP_LEFT = 0
TOP_RIGHT = 1
BOTTOM_LEFT = 2
BOTTOM_RIGHT = 3

RIGHT_WRIST = 4
LEFT_WRIST = 7


def extract_camera_video(reader, corner, duration, output_file=None):
    if output_file is None:
        output_file = 'crop.avi'
    crop = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (1280, 720))
    ret = 0
    for i in range(duration):
        ret, frame = reader.read()
        if not ret: break
        frame = extract_camera_frame(frame, corner)
        crop.write(frame)
    crop.release()
    return ret


def extract_camera_frame(frame, corner):
    if corner == TOP_LEFT:
        return frame[:720, :1280]
    elif corner == BOTTOM_LEFT:
        return frame[:720, -1280:]
    elif corner == TOP_RIGHT:
        return frame[-720:, :1280]
    elif corner == BOTTOM_RIGHT:
        return frame[-720:, -1280:]
    else:
        return None


def append_to_writer(stream, addition):
    vid_in = cv2.VideoCapture(addition)
    if not vid_in.isOpened():
        print("Error opening stream data")

    while vid_in.isOpened():
        ret, frame = vid_in.read()
        if not ret:
            break
        stream.write(frame)


def get_calibration_points(frame, corner, num_coords, default=None, can_select_none=False, scale_to=None):
    def return_mouse_event(event, x, y, _, param):
        param[0:3] = event, x, y

    calibration_coords = [None] * num_coords
    if default is not None:
        calibration_coords[:len(default)] = default
    coordinate_number = 0
    print("Please select {} calibration points using the mouse. "
          "Press [ENTER] after each point is selected to confirm."
          .format(num_coords))
    cv2.namedWindow("calibration")
    mouse_back = [0]
    cv2.setMouseCallback("calibration", return_mouse_event, mouse_back)
    while coordinate_number < num_coords:
        if mouse_back[0] == cv2.EVENT_LBUTTONDOWN:
            calibration_coords[coordinate_number] = mouse_back[1:3]
        elif mouse_back[0] == cv2.EVENT_RBUTTONDOWN:
            calibration_coords[coordinate_number] = None

        cropped_frame = extract_camera_frame(frame, corner).copy()
        if scale_to is not None:
            cropped_frame = scale_image(cropped_frame, (1280, 720), scale_to)
        for i, coord in enumerate(calibration_coords):
            if coord is not None:
                if i == coordinate_number:
                    color = (0, 0, 255)
                else:
                    color = (255, 0, 0)
                cropped_frame = cv2.circle(cropped_frame, tuple(coord), 0, color, 3)
        cv2.imshow("calibration", cropped_frame)

        if cv2.waitKey(1) == 13:
            if calibration_coords[coordinate_number] is not None:
                print("Calibration coordinate {} selected: {}"
                      .format(coordinate_number, tuple(calibration_coords[coordinate_number])))
                coordinate_number += 1
            else:
                if can_select_none:
                    print("Calibration coordinate stated to not be visible in image.")
                    coordinate_number += 1
                else:
                    print("Please use the mouse to select a coordinate on the image.")

    print()
    cv2.destroyWindow("calibration")
    return calibration_coords


def calibrate_marker_points(video_in, scale_to=None):
    coords = " "
    while coords and len(coords.split()) != 16:
        print("Please enter marker coordinates as 16 space separated integers, or leave empty to manually select.")
        coords = input()

    if not coords:
        ret, frame = video_in.read()
        if not ret:
            return
        video_in.set(cv2.CAP_PROP_POS_FRAMES, 0)

        top_left_coords = np.array(get_calibration_points(frame, TOP_LEFT, 4, scale_to=scale_to))
        bottom_right_coords = np.array(get_calibration_points(frame, BOTTOM_RIGHT, 4, scale_to=scale_to))
    else:
        coords = coords.split()
        top_left_coords = np.reshape([int(x) for x in coords[:8]], (4, 2))
        bottom_right_coords = np.reshape([int(x) for x in coords[8:]], (4, 2))
    return top_left_coords, bottom_right_coords


def clear_heatmap_folder(folder="output_heatmaps_folder"):
    for f in glob.glob(os.path.join(os.getcwd(), folder, "crop_*_pose_heatmaps.png")):
        os.remove(f)


class HeatmapVideoCapture(cv2.VideoCapture):
    def __init__(self, *args, heatmap_width=640, num_heatmaps=26):
        super().__init__(*args)
        self.heatmap_width = heatmap_width
        self.num_heatmaps = num_heatmaps
        self._buffer = None
        self._ret = None
        self._buffer_read = np.array([True] * num_heatmaps)
        self._skipped_frames = np.array([False] * num_heatmaps)
        self._joint_reader = {}
        self._child_reader = False

    def read(self, image=None):
        if self._ret:
            self._skipped_frames = ~self._buffer_read
            self._buffer_read &= False
        self._ret, self._buffer = super().read(image)
        if self._ret:
            self._buffer = cv2.cvtColor(self._buffer, cv2.COLOR_BGR2GRAY)
        return self._ret, self._buffer

    @property
    def left_wrist(self):
        return self.extract_joint_heatmap(LEFT_WRIST)

    @property
    def right_wrist(self):
        return self.extract_joint_heatmap(RIGHT_WRIST)

    def extract_joint_heatmap(self, joint, advance_buffer=False):
        if advance_buffer:
            if self._buffer_read[joint]:
                self.read()
            self._buffer_read[joint] = True
        elif self._skipped_frames[joint]:
            warn('Reader has fallen behind lead reader. Some frames may have been lost.', RuntimeWarning)
            self._skipped_frames[joint] = False
        if self._ret:
            return self._buffer[:, joint * self.heatmap_width: (joint + 1) * self.heatmap_width].copy()
        else:
            return None


def get_scale_factor(source, dest):
    if dest is None:
        scale_factor = 1
    else:
        scale_factor = dest[0] / source[0]
    # if source[0] / source[1] <= dest[0] / dest[1]:
    #     scale_factor = dest[0] / source[0]
    # else:
    #     scale_factor = dest[1] / source[1]
    return scale_factor


def scale_vector(vec, source, dest):
    if vec is None:
        return None
    scale_factor = get_scale_factor(source, dest)
    return int(vec[0] * scale_factor), int(vec[1] * scale_factor)


def scale_image(image, source, dest):
    scale_factor = get_scale_factor(source, dest)
    out = np.zeros((*dest[::-1],), np.uint8)
    scaled = cv2.resize(image, (0, 0), fx=scale_factor, fy=scale_factor)
    out[:dest[1], :dest[0]] = scaled[:dest[1], :dest[0]]
    return out
