import collections
import os
import pickle

import cv2
import numpy as np
import scipy.optimize as optim

import joint_tracker
import video_processing
import video_processing as vid

base_folder = os.path.normpath(r'C:\Users\ee18lz')
subject = ''
video = '2019-05-21 11-09-15.flv'


def parameter_score(parameters, output_data, tracker: joint_tracker.CrossPerspectiveTracker, start_frame, length):
    print("\tParameters: {}".format([round(p, 4) for p in parameters]))
    tracker.parameters = parameters
    try:
        tracker.reevaluate_history()
    except FloatingPointError:
        return 1e24
    score = 0
    for i in range(length):
        for estimate, actual in zip(tracker.get_wrist_position(start_frame + i), output_data[i]):
            if estimate is None or actual is None:
                if (estimate is None) != (actual is None):
                    score += 10
            else:
                score += (np.linalg.norm(np.array(actual) - estimate) ** 2) / 1000
    return score


def estimate_stats(estimate_data, output_data):
    dist = 0
    false_positive = 0
    false_negative = 0
    for i in range(len(estimate_data)):
        for estimate, actual in zip(estimate_data[i], output_data[i]):
            if estimate is None or actual is None:
                if estimate is not None and actual is None:
                    false_positive += 1
                elif estimate is None and actual is not None:
                    false_negative += 1
            else:
                dist += np.linalg.norm(np.array(actual) - estimate)
    dist /= len(estimate_data) * 4
    false_positive /= len(estimate_data) * 4
    false_negative /= len(estimate_data) * 4
    return dist, false_positive, false_negative


def load_input_data(tl_left_tracker, tl_right_tracker, br_left_tracker, br_right_tracker, length):
    def get_best_two_candidates(tracker, index):
        candidates = np.array(sorted(tracker.history[index], key=lambda x: -x[3]))
        output = np.zeros((2, 3))
        if len(candidates) > 0:
            output[:len(candidates)] = candidates[:2, 1:]
        output[0, :2] = video_processing.scale_vector(output[0, :2], tracker.dimensions, (1280, 720))
        output[1, :2] = video_processing.scale_vector(output[1, :2], tracker.dimensions, (1280, 720))
        return (*output[0], *output[1])

    input_data = np.zeros((length, 24))
    for i in range(length):
        input_data[i, :6] = get_best_two_candidates(tl_left_tracker, i)
        input_data[i, 6:12] = get_best_two_candidates(tl_right_tracker, i)
        input_data[i, 12:18] = get_best_two_candidates(br_left_tracker, i)
        input_data[i, 18:] = get_best_two_candidates(br_right_tracker, i)
    return input_data


def load_output_data(tracker: joint_tracker.CrossPerspectiveTracker, video_in, base_name, start_frame, length,
                     confirm_coordinates=False, step_size=10):
    if os.path.exists('{}_labelled_output.npy'.format(base_name)):
        labelled_output = np.load('{}_labelled_output.npy'.format(base_name))
        if not confirm_coordinates:
            return labelled_output
    if confirm_coordinates:
        existing_output = iter(np.load('{}_labelled_output.npy'.format(base_name)))
    labelled_output = collections.deque()
    for frame_pos in range(start_frame, start_frame + length + 1, step_size):
        video_in.set(cv2.CAP_PROP_POS_FRAMES, frame_pos)
        ret, frame = video_in.read()
        if not ret:
            break

        if confirm_coordinates:
            wrist_positions = next(existing_output)
        else:
            wrist_positions = tracker.get_wrist_position(index=frame_pos)

        br_coords = vid.get_calibration_points(
            frame, vid.BOTTOM_RIGHT, 2, [wrist_positions[0], wrist_positions[1]], can_select_none=True)
        tl_coords = vid.get_calibration_points(
            frame, vid.TOP_LEFT, 2, [wrist_positions[2], wrist_positions[3]], can_select_none=True)

        labelled_output.append((*br_coords, *tl_coords))
    labelled_output = np.array(labelled_output)
    np.save('{}_labelled_output.npy'.format(base_name), labelled_output)
    return labelled_output


def optimize():
    start_frame = 600
    length = 1201

    video_in = cv2.VideoCapture(os.path.join(base_folder, subject, video))
    tracker = joint_tracker.load_tracker()
    tracker.parameters = [0.1816006, 0.99652041, 0.71371094, 0.56773857, 0.34748925,
                          0.98643294, 29.52344033]
    tracker.reevaluate_history()

    output_data = load_output_data(tracker, video_in, None, start_frame, length).repeat(10, axis=0)
    # input_data = load_input_data(*trackers, length)

    bounds = [(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 100)]
    args = (output_data, tracker, start_frame, length)
    res = optim.differential_evolution(parameter_score, bounds, args, disp=True, popsize=50, strategy='rand1bin')
    pickle.dump(res, open('res.pkl', 'wb'))
    print(res)


if __name__ == '__main__':
    optimize()
