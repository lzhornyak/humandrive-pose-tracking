import collections
import os
import pickle
from typing import Tuple

import cv2
import numpy as np
from scipy import ndimage

import video_processing
import video_processing as vid
import visualization as vis


def track_wrists(source_heatmap_in, target_heatmap_in, base_name, video_in=None, parameters=None, start_frame=0,
                 length=None, use_existing_tracker=False, net_resolution=(640, 384)):
    if parameters is None:
        parameters = [1, 0, 0, 0, 0, 0, 0]
    if video_in is not None:
        video_in.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
        video1_out = cv2.VideoWriter('{}_example1.avi'.format(base_name),
                                     cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (1280, 720))
        video2_out = cv2.VideoWriter('{}_example2.avi'.format(base_name),
                                     cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (1280, 720))
    source_heatmap_in.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
    target_heatmap_in.set(cv2.CAP_PROP_POS_FRAMES, start_frame)

    source_points = np.float32([[243, 175], [356, 247], [356, 395], [227, 360]])
    target_points = np.float32([[555, 116], [689, 264], [584, 426], [425, 286]])
    source_points = (source_points * [net_resolution[0] / 864, net_resolution[1] / 512]).astype(np.float32)
    target_points = (target_points * [net_resolution[0] / 864, net_resolution[1] / 512]).astype(np.float32)

    if use_existing_tracker:
        tracker = load_tracker(base_name)
        tracker.parameters = parameters
        tracker.reevaluate_history()
    else:
        tracker = CrossPerspectiveTracker(
            source_heatmap_in, target_heatmap_in, parameters,
            source_points, target_points, base_name, dimensions=net_resolution
        )

    i = start_frame
    while source_heatmap_in.isOpened() and target_heatmap_in.isOpened():
        if not use_existing_tracker:
            ret_s, _ = source_heatmap_in.read()
            ret_t, _ = target_heatmap_in.read()
            if not ret_s or not ret_t:
                break
            tracker.next_frame()
            tracker.compute_secondary_scores()
            wrist_positions = tracker.get_wrist_position()
        else:
            try:
                wrist_positions = tracker.get_wrist_position(i)
            except IndexError:
                break

        if video_in is not None:
            ret_1, image = video_in.read()
            if not ret_1:
                video_in = None
                print("Could not read base video")
                continue
            source = image[-720:, -1280:]
            target = image[:720, :1280]
            print(video_in.get(cv2.CAP_PROP_POS_FRAMES), source_heatmap_in.get(cv2.CAP_PROP_POS_FRAMES))
            source = vis.overlay_heatmap(source,
                                         source_heatmap_in.extract_joint_heatmap(vid.LEFT_WRIST, use_existing_tracker) +
                                         source_heatmap_in.extract_joint_heatmap(vid.RIGHT_WRIST, use_existing_tracker),
                                         1)
            target = vis.overlay_heatmap(target,
                                         target_heatmap_in.extract_joint_heatmap(vid.LEFT_WRIST, use_existing_tracker) +
                                         target_heatmap_in.extract_joint_heatmap(vid.RIGHT_WRIST, use_existing_tracker),
                                         1)
            source = vis.overlay_wrist_position(source, wrist_positions[0], wrist_positions[1])
            target = vis.overlay_wrist_position(target, wrist_positions[2], wrist_positions[3])
            cv2.imshow("source", source)
            cv2.imshow("target", target)
            video1_out.write(source)
            video2_out.write(target)

            key = cv2.waitKey(1)
            if key == 27:
                break
            elif key == 32:
                while cv2.waitKey() != 32:
                    pass
        i += 1
        if length is not None and i >= start_frame + length:
            break

    tracker.lock_tracker()
    return tracker


class WristTracker:
    def __init__(self, heatmap, joint, parameters, line, opposite=None, output_scale=(1280, 720),
                 dimensions=(640, 384)):
        self.history = collections.deque()
        self._cross_vectors = collections.deque()
        self._item_order_update = collections.deque()
        self.heatmap = heatmap
        self.line = line
        self.opposite = opposite
        self.joint = joint
        self.dimensions = dimensions
        self.output_scale = output_scale
        self.parameters = parameters
        self._locked = False

    @property
    def locked(self):
        return self._locked

    def lock_tracker(self):
        self.history = list(self.history)
        self._cross_vectors = list(self._cross_vectors)
        self._item_order_update = list(self._item_order_update)
        self._locked = True

    def __len__(self):
        return len(self.history)

    def next_frame(self):
        if self.locked:
            print("Cannot move to next frame; tracking has been locked.")
        heatmap = self.heatmap.extract_joint_heatmap(self.joint, True)
        if heatmap is None:
            return
        threshold = self.threshold_joints(heatmap, 1)
        labelled, num_labels = ndimage.label(threshold)

        candidates = np.zeros((num_labels + 1, 8))
        if self.history:
            previous_pos = self._get_current_coordinates()
            candidates[0] = [0, *previous_pos, 0, 0, 0, 0, 0]  # null candidate; previous position

        for i in range(1, num_labels + 1):
            coord = ndimage.center_of_mass(heatmap, labelled, i)
            count = cv2.countNonZero((labelled == i).astype(np.uint8))
            candidates[i] = [i, *coord[::-1], np.sqrt(count), 0, 0, 0, 0]

        # heatmap = cv2.circle(heatmap, tuple(candidates[0][:2].astype(int)), 5, 0)
        # cv2.imshow("heatmap_{}".format(self.joint), heatmap)
        self.history.append(candidates)
        self._item_order_update = np.arange(len(candidates))

    def compute_secondary_scores(self, index=-1):
        self._interpret_history(index=index)
        self._distinguish_left_right(index=index)

    def finalize_scores(self, index=-1):
        # if self.opposite is not None:
        #     self._cross_compare(index=index)
        sorted_indices = np.argsort(np.sum(self.history[index][:, 3:], axis=1))[::-1]
        self.history[index] = self.history[index][sorted_indices]
        # self._cross_vectors[index] = self._cross_vectors[index][sorted_indices]
        self._item_order_update = sorted_indices

    def clear_secondary_scores(self, index=-1):
        for candidate in self.history[index]:
            if candidate[0] == 0 and index != 0 and len(self) > 1:
                candidate[1:3] = self.history[index - 1][0][1:3]
            candidate[4:] = 0

    def get_wrist_position(self, index=-1, scale_to=(1280, 720), return_score=False):
        if isinstance(index, (list, np.ndarray)):
            out = []
            for i in index:
                out.append(self.get_wrist_position(i, scale_to, return_score))
            return out
        pos, score = self._get_current_coordinates(return_score=True, index=index)
        if score <= 0:
            pos = None
        if scale_to is not None:
            pos = video_processing.scale_vector(pos, self.dimensions, scale_to)
        if return_score:
            return pos, score
        else:
            return pos

    def __getitem__(self, item):
        if isinstance(item, slice):
            out = []
            for index in range(item.start or 0, item.stop or len(self), item.step or 1):
                out.append(self[index])
            return out
        else:
            if self.history[item][0, 3:].sum() <= 0:
                return None
            else:
                return video_processing.scale_vector(self.history[item][0, 1:3], self.dimensions, self.output_scale)

    def get_cross_vectors(self, index=-1, store=False, transform=None):
        if len(self._cross_vectors) == len(self):
            return self._cross_vectors[index]
        else:
            s_coord = self.history[index][:, 1:3]
            o_coord = self.opposite.history[index][:, 1:3]
            if transform is not None:
                s_coord = cv2.perspectiveTransform(s_coord[np.newaxis], transform).reshape(-1, 2)
                o_coord = cv2.perspectiveTransform(o_coord[np.newaxis], transform).reshape(-1, 2)
                store = False
            cross_vecs = o_coord - np.transpose(s_coord[np.newaxis], (1, 0, 2))
            if store:
                self._cross_vectors.append(cross_vecs)
            return cross_vecs

    def reevaluate_history(self):
        if not self.locked:
            print("Locking recommend before reevaluation. Performance may be affected.")
        for i in range(len(self)):
            self.clear_secondary_scores(index=i)
            self.compute_secondary_scores(index=i)
            self.finalize_scores(index=i)

    def _interpret_history(self, index=-1):
        if index == 0 or len(self) < 2:
            return
        previous = self.history[index - 1]
        raw_memory_score = previous[:, 3] * self.parameters[0] + previous[:, 4] * self.parameters[1]
        update_dist = \
            np.linalg.norm(self.history[index][:, 1:3][np.newaxis].transpose(1, 0, 2) - previous[:, 1:3], axis=2) ** 2
        self.history[index][:, 4] = \
            np.max(raw_memory_score * np.maximum((1 - update_dist * self.parameters[2]), 0), axis=1)

    def _distinguish_left_right(self, index=-1):
        direction = self.line[1] - self.line[0]
        direction /= np.linalg.norm(direction)
        distance = ((self.history[index][:, 1:3] - self.line[0]) * direction).sum(axis=1)
        connection = self.history[index][:, 1:3] - (self.line[0] + distance[np.newaxis].T * direction)
        score = np.linalg.norm(connection, axis=1) * np.sign(np.cross(connection, direction))
        self.history[index][:, 7] = np.clip(score * self.parameters[5], -self.parameters[6], self.parameters[6])

    def _get_current_coordinates(self, return_score=False, index=-1):
        cur_candidate = self.history[index][0]
        if return_score:
            return self._get_candidate_coordinates(cur_candidate), self.candidate_score(cur_candidate)
        else:
            return self._get_candidate_coordinates(cur_candidate)

    @staticmethod
    def candidate_score(candidate):
        return np.sum(candidate[3:])

    @staticmethod
    def _get_candidate_coordinates(candidate):
        return tuple(candidate[1:3].astype(np.uint32))

    @staticmethod
    def threshold_joints(heatmap, thresh_to=255):
        _, heatmap = cv2.threshold(heatmap, 10, thresh_to, cv2.THRESH_TOZERO)
        _, heatmap = cv2.threshold(heatmap, 40, thresh_to, cv2.THRESH_OTSU)
        return heatmap


class CrossPerspectiveTracker:
    def __init__(self, source_heatmap_in, target_heatmap_in, parameters, source_markers, target_markers, base_name,
                 dimensions=(640, 384)):
        self._parameters = parameters
        self.source_markers = source_markers
        self.target_markers = target_markers
        self.transform_matrix = cv2.getPerspectiveTransform(source_markers, target_markers)
        self.dimensions = dimensions
        self.base_name = base_name

        source_line = np.array((vid.scale_vector((530, 584), (1280, 720), dimensions),
                                vid.scale_vector((354, 266), (1280, 720), dimensions)), float)
        target_line = np.array((vid.scale_vector((863, 632), (1280, 720), dimensions),
                                vid.scale_vector((816, 169), (1280, 720), dimensions)), float)
        self.source_trackers = [
            WristTracker(source_heatmap_in, vid.LEFT_WRIST, parameters, source_line, dimensions=dimensions),
            WristTracker(source_heatmap_in, vid.RIGHT_WRIST, parameters, source_line[::-1], dimensions=dimensions)]
        self.target_trackers = [
            WristTracker(target_heatmap_in, vid.LEFT_WRIST, parameters, target_line, dimensions=dimensions),
            WristTracker(target_heatmap_in, vid.RIGHT_WRIST, parameters, target_line[::-1], dimensions=dimensions)]
        self.source_trackers[0].opposite = self.source_trackers[1]
        self.source_trackers[1].opposite = self.source_trackers[0]
        self.target_trackers[0].opposite = self.target_trackers[1]
        self.target_trackers[1].opposite = self.target_trackers[0]

        self._similarities = collections.deque()
        self._differences = collections.deque()
        self._locked = False

    def __len__(self):
        return len(self._similarities)

    @property
    def locked(self):
        return self._locked

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, parameters):
        self._parameters = parameters
        self.source_trackers[0].parameters = parameters
        self.source_trackers[1].parameters = parameters
        self.target_trackers[0].parameters = parameters
        self.target_trackers[1].parameters = parameters

    @property
    def output_scale(self):
        return self.source_trackers[0].output_scale

    @output_scale.setter
    def output_scale(self, output_scale):
        self.source_trackers[0].output_scale = output_scale
        self.source_trackers[1].output_scale = output_scale
        self.target_trackers[0].output_scale = output_scale
        self.target_trackers[1].output_scale = output_scale

    def lock_tracker(self):
        self.source_trackers[0].lock_tracker()
        self.source_trackers[1].lock_tracker()
        self.target_trackers[0].lock_tracker()
        self.target_trackers[1].lock_tracker()
        self._differences = list(self._differences)
        self._similarities = list(self._similarities)
        self._locked = True

    def next_frame(self):
        self.source_trackers[0].next_frame()
        self.source_trackers[1].next_frame()
        self.target_trackers[0].next_frame()
        self.target_trackers[1].next_frame()

    def compute_secondary_scores(self, index=-1):
        self.source_trackers[0].compute_secondary_scores(index)
        self.source_trackers[1].compute_secondary_scores(index)
        self.target_trackers[0].compute_secondary_scores(index)
        self.target_trackers[1].compute_secondary_scores(index)
        self._compute_similarities(index)
        self._assign_similarity_scores(index)
        self.source_trackers[0].finalize_scores(index)
        self.source_trackers[1].finalize_scores(index)
        self.target_trackers[0].finalize_scores(index)
        self.target_trackers[1].finalize_scores(index)

        self._differences[index][0] = self._differences[index][0][self.source_trackers[0]._item_order_update]
        self._differences[index][0] = self._differences[index][0][:, self.target_trackers[0]._item_order_update]
        self._differences[index][1] = self._differences[index][1][self.source_trackers[1]._item_order_update]
        self._differences[index][1] = self._differences[index][1][:, self.target_trackers[1]._item_order_update]
        self._similarities[index][0] = self._similarities[index][0][self.source_trackers[0]._item_order_update]
        self._similarities[index][0] = self._similarities[index][0][:, self.target_trackers[0]._item_order_update]
        self._similarities[index][1] = self._similarities[index][1][self.source_trackers[1]._item_order_update]
        self._similarities[index][1] = self._similarities[index][1][:, self.target_trackers[1]._item_order_update]

    def clear_secondary_scores(self, index=-1):
        self.source_trackers[0].clear_secondary_scores(index)
        self.source_trackers[1].clear_secondary_scores(index)
        self.target_trackers[0].clear_secondary_scores(index)
        self.target_trackers[1].clear_secondary_scores(index)

    def reevaluate_history(self):
        if not self.locked:
            print("Locking recommend before reevaluation. Performance may be affected.")
        for i in range(len(self)):
            self.clear_secondary_scores(index=i)
            self.compute_secondary_scores(index=i)

    def get_wrist_position(self, index=-1, scale_to=(1280, 720)):
        return self.source_trackers[0].get_wrist_position(index, scale_to), \
               self.source_trackers[1].get_wrist_position(index, scale_to), \
               self.target_trackers[0].get_wrist_position(index, scale_to), \
               self.target_trackers[1].get_wrist_position(index, scale_to)

    def __getitem__(self, item):
        coords = self.source_trackers[0][item], self.source_trackers[1][item], \
                 self.target_trackers[0][item], self.target_trackers[1][item]
        if isinstance(item, slice):
            coords = list(zip(*coords))
        return coords

    def generate_idealized_motion(self):
        def transform_to_ideal(tracker: WristTracker, transformation, offset):
            points = np.array([hist[0, 1:3] if hist[0, 3:].sum() > 0 else [np.NaN, np.NaN] for hist in tracker.history])
            ideal = cv2.perspectiveTransform(points[np.newaxis], transformation)[0] + offset
            ideal[np.isnan(points).any(axis=1)] = [np.NaN, np.NaN]
            polar_ideal = np.vstack((np.linalg.norm(ideal, axis=1), np.arctan2(ideal[:, 1], ideal[:, 0]))).T
            return polar_ideal

        def weighted_mean(trackers: Tuple[WristTracker, WristTracker], coordinates):
            scores = [np.array([hist[0, 3:].sum() for hist in trackers[0].history]),
                      np.array([hist[0, 3:].sum() for hist in trackers[1].history])]
            proportion = scores[0] / (scores[0] + scores[1])
            average_coords = np.zeros((scores[0].shape[0], 2))
            average_coords[:, 0] = coordinates[0][:, 0] * proportion + coordinates[1][:, 0] * (1 - proportion)
            average_coords[:, 1] = np.arctan2(
                np.sin(coordinates[0][:, 1]) * proportion + np.sin(coordinates[1][:, 1]) * (1 - proportion),
                np.cos(coordinates[0][:, 1]) * proportion + np.cos(coordinates[1][:, 1]) * (1 - proportion)
            )
            average_coords[np.isnan(coordinates[0]).any(axis=1)] = coordinates[1][np.isnan(coordinates[0]).any(axis=1)]
            average_coords[np.isnan(coordinates[1]).any(axis=1)] = coordinates[0][np.isnan(coordinates[1]).any(axis=1)]
            return average_coords

        edge_coordinates = np.float32([[0, 1], [1, 0], [0, -1], [-1, 0]])
        source_to_ideal = cv2.getPerspectiveTransform(self.source_markers, edge_coordinates)
        target_to_ideal = cv2.getPerspectiveTransform(self.target_markers, edge_coordinates)
        source_polar = [transform_to_ideal(self.source_trackers[0], source_to_ideal, [-0.12, 0.48]),
                        transform_to_ideal(self.source_trackers[1], source_to_ideal, [-0.50, 0.54])]
        target_polar = [transform_to_ideal(self.target_trackers[0], target_to_ideal, [-0.05, 0.34]),
                        transform_to_ideal(self.target_trackers[1], target_to_ideal, [-0.27, 0.35])]
        average_polar = [
            weighted_mean((self.source_trackers[0], self.target_trackers[0]), (source_polar[0], target_polar[0])),
            weighted_mean((self.source_trackers[1], self.target_trackers[1]), (source_polar[1], target_polar[1]))]
        return np.array(average_polar)

    def save(self):
        self.lock_tracker()
        save = (self._differences, self._similarities, self.source_trackers[0].history, self.source_trackers[1].history,
                self.target_trackers[0].history, self.target_trackers[1].history, self.parameters,
                self.source_markers, self.target_markers, self.base_name, self.dimensions)
        pickle.dump(save, open('{}_tracker.pkl'.format(self.base_name), 'wb'))

    @staticmethod
    def load(base_name):
        data = np.load('{}_tracker.pkl'.format(base_name))
        self = CrossPerspectiveTracker(None, None, *data[6:])
        self._differences = data[0]
        self._similarities = data[1]
        self.source_trackers[0].history = data[2]
        self.source_trackers[1].history = data[3]
        self.target_trackers[0].history = data[4]
        self.target_trackers[1].history = data[5]
        self.lock_tracker()
        return self

    def _compute_similarities(self, index=-1):
        if len(self._similarities) == len(self.source_trackers[0]):
            differences = self._differences[index]
        else:
            source_positions = [cv2.perspectiveTransform(self.source_trackers[0].history[index][:, 1:3][np.newaxis],
                                                         self.transform_matrix).transpose(1, 0, 2),
                                cv2.perspectiveTransform(self.source_trackers[1].history[index][:, 1:3][np.newaxis],
                                                         self.transform_matrix).transpose(1, 0, 2)]
            target_positions = [self.target_trackers[0].history[index][:, 1:3],
                                self.target_trackers[1].history[index][:, 1:3]]
            differences = [np.linalg.norm((source_positions[0] - target_positions[0]), axis=2),
                           np.linalg.norm((source_positions[1] - target_positions[1]), axis=2)]
            self._differences.append(differences)
            self._similarities.append([])

        similarities = [self.parameters[3] / (differences[0] * self.parameters[4] + 1),
                        self.parameters[3] / (differences[1] * self.parameters[4] + 1)]
        similarities[0] *= self.source_trackers[0].history[index][:, 3][np.newaxis].T + \
                           self.target_trackers[0].history[index][:, 3]
        similarities[1] *= self.source_trackers[1].history[index][:, 3][np.newaxis].T + \
                           self.target_trackers[1].history[index][:, 3]
        self._similarities[index] = similarities

    def _assign_similarity_scores(self, index=-1):
        self.source_trackers[0].history[index][:, 5] = \
            self._similarities[index][0].sum(axis=1) / (self.source_trackers[0].history[index][:, 3] + 1e6)
        self.source_trackers[1].history[index][:, 5] = \
            self._similarities[index][1].sum(axis=1) / (self.source_trackers[1].history[index][:, 3] + 1e6)
        self.target_trackers[0].history[index][:, 5] = \
            self._similarities[index][0].sum(axis=0) / (self.target_trackers[0].history[index][:, 3] + 1e6)
        self.target_trackers[1].history[index][:, 5] = \
            self._similarities[index][1].sum(axis=0) / (self.target_trackers[1].history[index][:, 3] + 1e6)

    def _assign_similarity_scores_(self, index=-1):
        def condense_scores_along_dimension(scores, dimension):
            condensed_axes = tuple(set(range(4)).difference((dimension,)))
            return scores.max(axis=condensed_axes)

        self.source_trackers[0].history[index][:, 5] = condense_scores_along_dimension(self._similarities[index], 0)
        self.source_trackers[1].history[index][:, 5] = condense_scores_along_dimension(self._similarities[index], 1)
        self.target_trackers[0].history[index][:, 5] = condense_scores_along_dimension(self._similarities[index], 2)
        self.target_trackers[1].history[index][:, 5] = condense_scores_along_dimension(self._similarities[index], 3)


def load_tracker(base_name):
    if os.path.exists('{}_tracker.pkl'.format(base_name)):
        tracker = CrossPerspectiveTracker.load(base_name)
    else:
        print("Running heatmap analysis")
        tracker = track_wrists(vid.HeatmapVideoCapture(
            '{}BR.avi'.format(base_name)), vid.HeatmapVideoCapture('{}TL.avi'.format(base_name)), base_name)
        tracker.save()
    return tracker
