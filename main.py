import argparse
import os

import cv2
import numpy as np

import openpose as pose
import video_processing as vid
import visualization as vis
import joint_tracker

parser = argparse.ArgumentParser(description='Identify the wrists in the provided video.')
parser.add_argument('mode')

parser.add_argument('--video_file', '--vid', default='video')
parser.add_argument('--input_file', '--in', default='video')
parser.add_argument('--output_file', '--out', default='video')
parser.add_argument('--net_resolution', type=int, nargs=2, default=[640, 384])
parser.add_argument('--skip_checks', action='store_true')


def generate_all_heatmaps(video_in, base_name, net_resolution=(640, 384), skip_checks=False):
    if (os.path.exists("{}TL.avi".format(base_name)) or os.path.exists("{}BR.avi".format(base_name))) \
            and not skip_checks:
        selected = "None"
        while selected != "y" and selected != "n":
            print("Detected existing heatmaps, are you sure you wish to continue? "
                  "Doing so will overwrite the existing. [y/n]")
            selected = input().lower()
        if selected == 'n':
            return

    print("\nGenerating heatmaps for Top Left camera.")
    pose.generate_heatmaps(video_in, vid.TOP_LEFT, "{}TL.avi".format(base_name),
                           net_resolution=net_resolution, save_keypoints=True)
    video_in.set(cv2.CAP_PROP_POS_FRAMES, 0)
    print("\nGenerating heatmaps for Bottom Right camera.")
    pose.generate_heatmaps(video_in, vid.BOTTOM_RIGHT, "{}BR.avi".format(base_name),
                           net_resolution=net_resolution, save_keypoints=True)
    video_in.set(cv2.CAP_PROP_POS_FRAMES, 0)
    # print("\nGenerating heatmaps for Bottom Left camera.")
    # pose.generate_heatmaps(video_in, vid.BOTTOM_LEFT, "heatmapBL.avi")


if __name__ == "__main__":
    args = parser.parse_args()
    video_in = cv2.VideoCapture(args.video_file)
    if not video_in.isOpened():
        raise IOError("Error opening video file '{}'.".format(args.video_file))

    if args.mode == 'generate':
        generate_all_heatmaps(video_in, args.output_file,
                              net_resolution=args.net_resolution, skip_checks=args.skip_checks)
    elif args.mode == 'detect':
        video_in.set(cv2.CAP_PROP_POS_FRAMES, 0)
        # vid.calibrate_marker_points(video_in)
        target_heatmap_in = vid.HeatmapVideoCapture("{}TL.avi".format(args.input_file))
        source_heatmap_in = vid.HeatmapVideoCapture("{}BR.avi".format(args.input_file))
        tracker = joint_tracker.track_wrists(source_heatmap_in, target_heatmap_in, args.input_file, video_in,
                                             parameters=[0.21848123, 0.25430643, 0.15675928, 0.76080355, 0.52323856,
                                                         0.98850334, 41.32774586],
                                             start_frame=0, length=None, net_resolution=args.net_resolution,
                                             use_existing_tracker=True)
        ideal_motion = tracker.generate_idealized_motion()
        np.savetxt(
            args.output_file,
            np.array([ideal_motion[0, :, 0], ideal_motion[0, :, 1], ideal_motion[1, :, 0], ideal_motion[1, :, 1]]).T)
    else:
        raise ValueError("Invalid mode '{}'.".format(args.mode))
