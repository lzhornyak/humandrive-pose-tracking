import glob
import json
import os
import pickle
import subprocess
import shutil

import cv2

import video_processing as vid


def generate_heatmaps(raw_in, corner, heatmap_file, net_resolution=(640, 384), save_keypoints=False):
    heatmap_out = cv2.VideoWriter(
        heatmap_file, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30,
        (net_resolution[0] * 26, net_resolution[1] * 1), isColor=False
    )

    i = 0
    data = []
    while raw_in.isOpened():
        print("Cropping Segment {} ... ".format(i), end='', flush=True)
        i += 1
        ret = vid.extract_camera_video(raw_in, corner, 1000)

        print("Running OpenPose ... ", end='', flush=True)
        if not save_keypoints:
            run_openpose(heatmap_folder="output_heatmaps", net_resolution=net_resolution)
        else:
            run_openpose(heatmap_folder="output_heatmaps", net_resolution=net_resolution, json_folder="output_json")
            data = load_json(json_folder="output_json", append_to=data)
            shutil.rmtree("output_json")

        print("Appending Heatmaps ... ")
        vid.append_to_writer(heatmap_out, r"output_heatmaps\crop_%012d_pose_heatmaps.png")
        shutil.rmtree("output_heatmaps")

        if not ret:
            break
    if save_keypoints:
        pickle.dump(data, open('{}_keypoints.pkl'.format(heatmap_file.split('.')[0]), 'wb'))


def load_json(json_folder="output_json", append_to=None):
    if append_to is None:
        data = []
    else:
        data = append_to
    for json_file in sorted(glob.glob("{}/*.json".format(json_folder))):
        with open(json_file) as file:
            datum = json.load(file)
            if len(datum['people']) > 0:
                data.append(datum['people'][0]['pose_keypoints_2d'])
            else:
                data.append(None)
    return data


def load_wrist_keypoints(base_name="heatmap"):
    def load_keypoints(name):
        with open(name, 'rb') as file:
            data = [[x[vid.LEFT_WRIST * 3:vid.LEFT_WRIST * 3 + 2], x[vid.RIGHT_WRIST * 3:vid.RIGHT_WRIST * 3 + 2]]
                    if x is not None else [None, None] for x in pickle.load(file)]
            return [x if x != [[0, 0], [0, 0]] else [None, None] for x in data]

    br_data = load_keypoints("{}BR_keypoints.pkl".format(base_name))
    tl_data = load_keypoints("{}TL_keypoints.pkl".format(base_name))
    # with open("{}BR_keypoints.pkl".format(base_name), 'rb') as file:
    #     br_data = [[x[vid.LEFT_WRIST*3:vid.LEFT_WRIST*3+2], x[vid.RIGHT_WRIST*3:vid.RIGHT_WRIST*3+2]]
    #                if x is not None else [None, None] for x in pickle.load(file)]
    #     br_data = [x if x != [[0, 0], [0, 0]] else [None, None] for x in br_data]
    # with open("{}TL_keypoints.pkl".format(base_name), 'rb') as file:
    #     tl_data = [[x[vid.LEFT_WRIST*3:vid.LEFT_WRIST*3+2], x[vid.RIGHT_WRIST*3:vid.RIGHT_WRIST*3+2]]
    #                if x is not None else [None, None] for x in pickle.load(file)]
    #     tl_data = [x if x != [[0, 0], [0, 0]] else [None, None] for x in tl_data]
    return [[*x, *y] for x, y in zip(br_data, tl_data)]


def run_openpose(video="crop.avi", heatmap_folder="output_heatmaps", net_resolution=(640, 384), json_folder=None,
                 hand=False):
    odir = os.getcwd()
    # os.chdir(r"C:\Users\ee18lz\OpenPose\openpose-1.4.0-win64-gpu-binaries")
    os.chdir(r"D:\Users\lzhor\OpenPose")
    subprocess.run(r"bin\OpenPoseDemo.exe --video {} {}"
                   " --heatmaps_add_parts --heatmaps_add_bkg --write_heatmaps " "{}"
                   ' --number_people_max 1 --net_resolution "{}x{}" --scale_number 3 --scale_gap 0.25'
                   "{}".format(os.path.join(odir, video),
                               "--write_json " + os.path.join(odir, json_folder) if json_folder is not None else '',
                               os.path.join(odir, heatmap_folder), net_resolution[0], net_resolution[1],
                               '--hand' if hand else ''),
                   stdout=open(os.devnull, 'wb'))
    os.chdir(odir)
