import cv2
import numpy as np

import joint_tracker
import video_processing as vid
from video_processing import scale_image


def overlay_heatmap(image, heatmap, method=0):
    heatmap = scale_image(heatmap, heatmap.shape[1::-1], image.shape[1::-1])
    if method == 0:
        heatmap = cv2.cvtColor(heatmap, cv2.COLOR_GRAY2BGR)
        combined = cv2.addWeighted(image, 1, heatmap, 0.5, 0)
    elif method == 1 or method == 2:
        if method == 2:
            heatmap = cv2.addWeighted(heatmap, 1.4, 0, 0, -60)
        heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_PARULA)
        # heatmap = cv2.cvtColor(heatmap, cv2.COLOR_GRAY2BGR)
        combined = cv2.addWeighted(image, 0.5, heatmap, 0.5, 0)
    elif method == 3:
        heatmap = joint_tracker.HeatmapRecorder.threshold_joints()
        heatmap = cv2.cvtColor(heatmap, cv2.COLOR_GRAY2BGR)
        combined = cv2.addWeighted(image, 1, heatmap, 0.5, 0)
    else:
        combined = None
    return combined


def overlay_wrist_position(image, left_wrist_pos, right_wrist_pos, inplace=False, size=5):
    if not inplace:
        image = image.copy()
    if left_wrist_pos is not None:
        image = cv2.circle(image, tuple(left_wrist_pos), 0, (255, 0, 0), 5)
    if right_wrist_pos is not None:
        image = cv2.circle(image, tuple(right_wrist_pos), 0, (0, 255, 0), 5)
    return image


def visualize_wrists():
    raw_in = cv2.VideoCapture(r'C:\Users\ee18lz\2019-05-21 11-09-15.flv')
    heat_in = cv2.VideoCapture(r'heatmap.avi')
    raw_in.set(cv2.CAP_PROP_POS_FRAMES, 500)
    heat_in.set(cv2.CAP_PROP_POS_FRAMES, 500)

    left_wrist_tracker = joint_tracker.WristTracker(heat_in)
    right_wrist_tracker = joint_tracker.WristTracker((640, 384))
    while raw_in.isOpened() and heat_in.isOpened():
        ret1, image = raw_in.read()
        ret2, heatmap = heat_in.read()
        if not (ret1 and ret2):
            break

        right_wrist = cv2.cvtColor(vid.extract_openpose_heatmap(heatmap, 4), cv2.COLOR_BGR2GRAY)
        right_wrist = cv2.warpAffine(right_wrist, np.float32([[1, 0, 0], [0, 1, 20]]), right_wrist.shape[1::-1])
        left_wrist = cv2.cvtColor(vid.extract_openpose_heatmap(heatmap, 7), cv2.COLOR_BGR2GRAY)
        left_wrist = cv2.warpAffine(left_wrist, np.float32([[1, 0, 0], [0, 1, 20]]), left_wrist.shape[1::-1])

        left_labelled = left_wrist_tracker.add_frame(left_wrist)
        right_labelled = right_wrist_tracker.add_frame(right_wrist)
        left_wrist_tracker._interpret_history()
        right_wrist_tracker._interpret_history()
        joint_tracker.WristTracker._cross_compare(left_wrist_tracker, left_labelled, right_wrist_tracker, right_labelled)

        heatmap = cv2.add(right_wrist, left_wrist)
        # crop = image[-720:, -1280:]
        crop = image[:720, :1280]
        combined = overlay_heatmap(crop, joint_tracker.WristTracker.threshold_joints(heatmap), 0)

        left_wrist_pos = left_wrist_tracker.get_wrist_position()
        right_wrist_pos = right_wrist_tracker.get_wrist_position()
        combined = cv2.circle(combined, left_wrist_pos, 5, 0)
        combined = cv2.circle(combined, right_wrist_pos, 5, 0)

        TL_points = np.float32([[849, 410], [635, 448], [863, 634], [952, 234]])
        BR_points = np.float32([[457, 441], [350, 557], [529, 584], [452, 277]])
        # TL_points = np.float32([[670, 380], [730, 540], [935, 520], [975, 365]])
        # BR_points = np.float32([[340, 485], [430, 585], [520, 490], [500, 365]])

        # raw_points = BR_points
        raw_points = TL_points
        combined = cv2.circle(combined, tuple(raw_points[0]), 0, (255, 0, 0), 5)
        combined = cv2.circle(combined, tuple(raw_points[1]), 0, (255, 0, 0), 5)
        combined = cv2.circle(combined, tuple(raw_points[2]), 0, (255, 0, 0), 5)
        combined = cv2.circle(combined, tuple(raw_points[3]), 0, (255, 0, 0), 5)

        # M = cv2.getPerspectiveTransform(BR_points, TL_points)
        M = cv2.getPerspectiveTransform(TL_points, BR_points)
        scaled = cv2.warpPerspective(combined, M, (1280, 720))

        cv2.imshow("scaled", scaled)
        cv2.imshow("combined", combined)

        image2 = image[-720:, -1280:]
        raw_points = BR_points
        image2 = cv2.circle(image2, tuple(raw_points[0]), 0, (255, 0, 0), 5)
        image2 = cv2.circle(image2, tuple(raw_points[1]), 0, (255, 0, 0), 5)
        image2 = cv2.circle(image2, tuple(raw_points[2]), 0, (255, 0, 0), 5)
        image2 = cv2.circle(image2, tuple(raw_points[3]), 0, (255, 0, 0), 5)
        cv2.imshow("image", image2)
        M = cv2.getPerspectiveTransform(BR_points, TL_points)
        image2 = cv2.warpPerspective(image2, M, (1280, 720))
        cv2.imshow("image2", image2)

        if cv2.waitKey(1) == 27: break


if __name__ == '__main__':
    visualize_wrists()
